/**
 *  Ce programme permet de réceptionner une trame reçu sur le port série (115200 bauds).
 *  Le format d'une trame est :
 * 			LxUn_Texte_A_Afficherz
 *		ou 
 *			Ez
 *		 Où :
 *			- L : le caractère 'L'
 *			- x : un chiffre entre 1 et 3
 *			- Un_Texte_A_Afficher : texte de longueur variable
 *                             peut contenir uniquement les caractères '0' à '9', 'a' à 'z', 'A' à 'Z', espace, '!', '?', '.', ',', ';'
 *			- z : \r ou \n (marqueur de fin de trame)
 *			- E : le caractère 'E'
 *
 *	Un REGEX permet de vérifier la validité de la trame
 *	En cas de réception du caractère E seul, le programme fait un ECHO (il le renvoie)
 *  Les caractères saut de ligne LF et retour chariot CR sont ignorés (\r et \n)
 *
 *	ToDo pour les étudiants :
 * 		Afficher sur l'écran LCD du M5 le texte reçu dans la trame (Un_Texte_A_Afficher)
 *		sur la ligne x (x : 2ème caractère de la trame)
 */	

#include <M5Core2.h>
#undef min  // https://github.com/m5stack/M5Stack/issues/97
#include <iostream>
#include <regex>
#include <string>

#define TRAME_SIZE  40

// Déclaration des fonctions
void Serial_callback();
void taskTraiteTrame(void *pvParameters);

// Déclration des variables globales
static QueueHandle_t queueReceptionSerie;

// SETUP *****************************************************************
void setup() {
    M5.begin(); // Initialise le port série du M5Stack à 115200 bauds

    // Initialisation d'une file de 40 char
    queueReceptionSerie = xQueueCreate(TRAME_SIZE, sizeof(char));

    xTaskCreatePinnedToCore(taskTraiteTrame,    // Function
                          "traiteTrame",        // Name
                          8192,        // Stack size
                          nullptr,     // Parameters
                          2,           // Priority
                          nullptr,     // Task handle
                          1);          // Core

    // Affectation de la fonction de callBack à l'évènement OnReceive
    // du port série
    Serial.onReceive(Serial_callback); 
} 

// LOOP ******************************************************************
void loop() {  
    // Tout se fait dans les tâches et/ou callback
}

/**************************************************************************
 * Lit les caractères reçus sur le port série et les ajoute dans 
 * la file "queueReceptionSerie"
 */
void Serial_callback() {
  char c;
  while(Serial.available() > 0) {   // Si au moins 1 caractère reçu
    Serial.read(&c, 1);             // Le lire
    xQueueSendToBack(queueReceptionSerie, (void *)&c, portMAX_DELAY);    // L'envoyer dans la file
  }
}

/**************************************************************************
 * Tâche de traitement de la trame :
 *      Trames vides (contenant juste \n et/ou \r) ignorées 
 *      Trames contenant juste "E" => echo
 *      Trames match "L[1-3][ !?.,;0-9a-zA-Z]{1,}" => 
 *          ToDo : Afficher sur la ligne 1, 2 ou 3 du LCD la partie [ !?.,;0-9a-zA-Z]{1,}
 *                 en transmettant le message à une autre tâche par une file (queue)
 */
void taskTraiteTrame(void *pvParameters) {
    char buffer[TRAME_SIZE];
    uint8_t i = 0;
    
    while(1) {
        if(xQueueReceive(queueReceptionSerie, (void *)&buffer[i], 1)) { // On stock les caractères reçu dans "buffer"
            if(buffer[i] == '\r' || buffer[i] == '\n') { // \r ou \n marquent la fin de la trame
                buffer[i] = '\0';
                if(buffer[0] == 'E' && i == 1) {  // Echo, la trame contient juste E
                    Serial.println("E");
                } else if (buffer[0] == '\0') { // Ignore les trames vides qui contiennent juste \r ou \n
                    i = 0;
                } else if (std::regex_match(std::string(buffer), std::regex("L[1-3][ !?.,;0-9a-zéèàêA-Z]{1,}"))) {
                    Serial.printf("Ajout ligne %u : %s\n", buffer[1] - '0', &buffer[2]); // Renvoie de la trame décodée
                    // ToDo pour les étudiants :
                    //      Afficher sur l'écran LCD du M5 le texte reçu dans la trame
                    //      sur la ligne x (x : 2ème caractère de la trame)
                }
                else {
                    Serial.println("ERROR");
                }
                i = 0;
            } else {
                if(++i > TRAME_SIZE - 1) i=0; // Attention à ne pas dépasser 39 !
            }
        }
    }
}